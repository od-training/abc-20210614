# Angular Boot Camp

[Angular Boot Camp Curriculum](https://github.com/angularbootcamp/abc)

[Angular Boot Camp Zip File](http://angularbootcamp.com/abc.zip)

[Video Manager](http://videomanager.angularbootcamp.com)

[Workshop Repository](https://bitbucket.org/od-training/abc-20210614/src/main/)

[Video Data](https://api.angularbootcamp.com/videos)

[Class Survey](https://angularbootcamp.com/survey)

## Resources

[DOM Events](https://developer.mozilla.org/en-US/docs/Web/Events)

[Angular CLI talk](https://www.youtube.com/watch?v=LKY0oc5snI4)

[Updating Angular](https://update.angular.io/)

[Angular Unit Testing](https://www.youtube.com/watch?v=xJ45MGDAi6c)

["Crying Baby" Talk on Angular State Management](https://www.youtube.com/watch?v=eBLTz8QRg4Q)

## Observables

[Cory Rylan Video on Observables](https://www.youtube.com/watch?v=-yY2ECd2tSM)

[Ben Lesh Video on creating Observables](https://www.youtube.com/watch?v=m40cF91F8_A)

[Book: Build Reactive Websites with RxJS](https://pragprog.com/book/rkrxjs/build-reactive-websites-with-rxjs)

[I switched a map and you'll never guess what happened next (SwitchMap video)](https://www.youtube.com/watch?v=rUZ9CjcaCEw)

[RXMarbles](http://rxmarbles.com)

[LearnRXJS](https://www.learnrxjs.io/)

[Seven Operators to Get Started with RxJS (Article)](https://www.infoq.com/articles/rxjs-get-started-operators)

[Reactive Visualizations](https://reactive.how/)

[Operator Decision Tree](https://rxjs-dev.firebaseapp.com/operator-decision-tree)

## Utilities

[json2ts (generate TypeScript interfaces from JSON)](http://json2ts.com/)
